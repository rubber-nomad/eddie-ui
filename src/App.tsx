import React from 'react';
import './App.css';
import {MapContainer, Marker, Popup, TileLayer} from "react-leaflet";

function App() {
    return (
        <div className="eddie">
            <header className="eddie-header">
                <h1>Eddie</h1>
            </header>
            <body>
            <div id='map' className='map'>
                <MapContainer center={[51.505, -0.09]} zoom={13} scrollWheelZoom={false}>
                    <TileLayer
                        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    />
                    <Marker position={[51.505, -0.09]}>
                        <Popup>
                            A pretty CSS3 popup. <br/> Easily customizable.
                        </Popup>
                    </Marker>
                </MapContainer>
            </div>
            </body>
        </div>
    );
}

export default App;
